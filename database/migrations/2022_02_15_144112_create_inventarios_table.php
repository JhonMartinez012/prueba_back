<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            //Campos foraneos
            $table->unsignedBigInteger('id_bodega');
            $table->unsignedBigInteger('id_producto');
            $table->integer('cantidad');
            $table->timestamps();
            //Campos foraneos
            $table->foreign('id_bodega')->references('id')->on('bodegas');
            $table->foreign('id_producto')->references('id')->on('productos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventarios');
    }
}
