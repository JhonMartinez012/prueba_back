<?php

namespace Database\Factories;

use App\Models\Bodega;
use App\Models\Producto;
use Illuminate\Database\Eloquent\Factories\Factory;

class InventarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_bodega' => Bodega::factory(),
            'id_producto' => Producto::factory(),
            'cantidad' => $this->faker->randomNumber(),
        ];
    }
}
