<?php

namespace App\Http\Controllers;

use App\Models\Inventario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            return DB::transaction(function () use ($request) {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'id_bodega' => 'required | integer| gt:0',
                        'id_producto' => 'required | integer| gt:0',
                        'cantidad' => 'required | integer| gt:0',

                    ],
                    [
                        'id_bodega.required' => 'Bodega obligatoria',
                        'id_producto.required' => 'Producto obligatorio',
                        'cantidad.required' => 'Cantidad requerida'
                    ]
                );
                if ($validator->fails()) {
                    return response()->json([
                        'success' => false,
                        'errores' => $validator->errors()
                    ], 200);
                }

                $existe = DB::table('inventarios')
                    ->select('id', 'id_bodega', 'id_producto')
                    ->where('id_bodega', '=', $request->id_bodega)
                    ->where('id_producto', "=", $request->id_producto)
                    ->first();


                if (!$existe) {
                    $inventario = Inventario::create([
                        'id_bodega' => $request->id_bodega,
                        'id_producto' => $request->id_producto,
                        'cantidad' => $request->cantidad,
                    ]);
                    return response()->json([
                        'success' => true,
                        'message' => 'Inventario registrada correctamente!',
                        'inventario' => $inventario,
                        'id' => $inventario->id,

                    ], 201);
                } else {

                    $inventario =  Inventario::find($existe->id);
                    //return $inventario;

                    $cantSuma = $inventario->cantidad + $request->cantidad;
                    $inventario = $inventario->update([
                        'id_bodega' => $request->id_bodega,
                        'id_producto' => $request->id_producto,
                        'cantidad' => $cantSuma,
                    ]);
                    return response()->json([
                        'success' => true,
                        'message' => 'Inventario actualizado correctamente!',
                        'inventario' => $inventario,


                    ], 201);
                }

                /*   */
            }, 5);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function show(Inventario $inventario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventario $inventario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventario $inventario)
    {
        //
    }

    public function transladoProducto(Request $request)
    {

        try {
            return DB::transaction(function () use ($request) {
                $inventarioOrigen = DB::table('inventarios')
                    ->select('inventarios.*')
                    ->where('id_bodega', '=', $request->id_bodega_origen)
                    ->where('id_producto', '=', $request->id_producto)
                    ->first();
                

                $inventarioDestino = DB::table('inventarios')
                    ->select('inventarios.*')
                    ->where('id_bodega', '=', $request->id_bodega_destino)
                    ->where('id_producto', '=', $request->id_producto)
                    ->first();
                
                if ($inventarioOrigen != []) {
                    if ($inventarioOrigen->cantidad >= $request->cantidad ) {
                       $bodegaDestino= DB::table('bodegas')
                    }
                }


                

            }, 5);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
