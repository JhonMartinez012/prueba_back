<?php

namespace App\Http\Controllers;

use App\Models\Inventario;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            return DB::transaction(function () use ($request) {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'nombreP' => 'required|string|max:50',
                        'descripcion' => 'required|string|max:300',
                        'cantidad' => 'integer|gt:0'
                        
                    ],
                    [
                        'nombreP.required' => 'Campo nombre es obligatorio',
                        'nombreP.max' => 'Solo puede ingresar maximo 50 caracteres',

                        'descripcion.required' => 'Debe haber un responsable para la bodega',
                        'descripcion.max' => 'Solo puede ingresar maximo 300 caracteres'

                    ]
                );
                if ($validator->fails()) {
                    return response()->json([
                        'success' => false,
                        'errores' => $validator->errors()
                    ], 200);
                }

               
                $producto = Producto::create([
                    'nombreP' => $request->nombreP,
                    'descripcion' => $request->descripcion,                        
                ]);

               
                $idProducto = $producto->id;
                $idBodega = 21;
               
                $inventario = Inventario::create([
                    'id_bodega' => $idBodega,
                    'id_producto' => $idProducto,  
                    'cantidad' =>  $request->cantidad,                    
                ]);
                return response()->json([
                    'success' => true,
                    'message' => 'producto registrado en el inventario correctamente!',
                    'inventario' => $inventario,                               

                ], 201);
               
                
                

            }, 5);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        //
    }

    public function listarProductosTotal()
    {
       try {
           $productos = DB::table('inventarios')
                          ->join('productos','inventarios.id_producto','productos.id')
                          ->select('inventarios.cantidad','productos.id AS id_product','productos.nombreP')
                          ->get()
                          ->groupBy('id_product')
                          ->values()->map(function($value, $key){
                              $total = 0;
                              foreach($value as $iterable) {
                                $total+= $iterable->cantidad;
                              }
                              return ['id_product'=>$value[0]->id_product, 'total'=>$total];
                          });          


           return response()->json(['productos' => $productos]);
       } catch (\Throwable $th) {
           throw $th;
       }
    } 
}
