<?php

namespace App\Http\Controllers;

use App\Models\Bodega;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BodegaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $bodegas = DB::table('bodegas')
                        ->orderBy('nombreB','asc')
                        ->get();


            return response()->json([
                'success'=>true,
                'bodegas'=>$bodegas,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Para poder registrar una nueva bodega hay que ingresar el nombre de la bodega y el responsable de ella 
        try {
            return DB::transaction(function () use ($request) {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'nombreB' => 'required',
                        'id_responsable' => 'required | integer| gt:0',
                        
                    ],
                    [
                        'nombreB.required' => 'Campo nombre es obligatorio',
                        'id_responsable.required' => 'Debe haber un responsable para la bodega'
                    ]
                );
                if ($validator->fails()) {
                    return response()->json([
                        'success' => false,
                        'errores' => $validator->errors()
                    ], 200);
                }

               /*  $usuario = DB::table('users')
                             ->select('id')
                             ->where('id',"=", $request->id_responsable)
                             ->get();
                
 */
                /* if ($usuario== $request->id_responsable) {
                }else{
                    return "paila";
                } */
                $bodega = Bodega::create([
                    'nombreB' => $request->nombreB,
                    'id_responsable' => $request->id_responsable,                        
                ]);
                return response()->json([
                    'success' => true,
                    'message' => 'Bodega registrada correctamente!',
                    'Bodega' => $bodega,
                    'id' => $bodega->id,

                ], 201);
            }, 5);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function show(Bodega $bodega)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bodega $bodega)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bodega $bodega)
    {
        //
    }

    public function listarBodegas(){
       
    }
}
