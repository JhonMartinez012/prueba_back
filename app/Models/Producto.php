<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public $table= 'productos';
    use HasFactory;

    protected $fillable = [
        'nombreP',
        'descripcion',
        'estado',       
    ];

    public function inventarios()
    {
        return $this->hasMany('App\Models\Inventario');
    }
}
