<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bodega extends Model
{
    public $table= 'bodegas';
    use HasFactory;

    protected $fillable = [
        'nombreB',
        'id_responsable',
        'estado',
       
    ];

    // relacion de uno a muchos con Inventario
    public function inventarios()
    {
        return $this->hasMany('App\Models\Inventario');
    }

    // ****** Relacion uno a muchos inverso con Usuario ********
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
