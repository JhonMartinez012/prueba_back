<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    public $table= 'inventarios';
    use HasFactory;

    protected $fillable = [
        'id_bodega',
        'id_producto',
        'cantidad',       
    ];


     // ****** Relacion uno a muchos inverso con Bodega ********
     public function bodega()
     {
         return $this->belongsTo('App\Models\Bodega');
     }

    // ****** Relacion uno a muchos inverso con Producto ********
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto');
    }

}
