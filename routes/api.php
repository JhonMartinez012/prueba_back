<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' =>'api',
    'prefix' => 'api',
    
], function ($router) {
    //rutas para el estadio 
    Route::get('listar-bodegas', 'App\Http\Controllers\BodegaController@index');
    Route::post('crear-bodega' , 'App\Http\Controllers\BodegaController@store');
                
    Route::post('crear-producto' , 'App\Http\Controllers\ProductoController@store');// ruta para insertar un producto e insertarlo en el inventario
    
    
    Route::get('listar-productos', 'App\Http\Controllers\ProductoController@listarProductosTotal'); // Listar los productos agregando el campo total
    
    Route::post('registrar-inventario' , 'App\Http\Controllers\InventarioController@store'); // crear o actualizar un registro en la tab inventarios

    Route::post('transladar-producto', 'App\Http\Controllers\InventarioController@transladoProducto'); // ruta para poder hacer el translado de una cantidad de un producto a otra bodega

});
